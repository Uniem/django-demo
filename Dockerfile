FROM python:3.10-slim-buster

ENV PYTHONUNBUFFERED 1

WORKDIR /app

ADD django-demo/requirements.txt requirements.txt

RUN pip install -r requirements.txt

CMD "./startup.sh"