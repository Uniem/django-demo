import os
from django.apps import apps
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'main.settings')

app = Celery('main', include=['jokes.tasks'])

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks(lambda: [n.name for n in apps.get_app_configs()])


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    from jokes.tasks import save_random_joke
    sender.add_periodic_task(10.0, save_random_joke.s('hello'))

@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')