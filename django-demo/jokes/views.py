from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from jokes.models import Joke
from jokes.serializers import JokeSerializer



class JokeList(APIView):

    def get(self, request):
        jokes = Joke.objects.all()
        serializer = JokeSerializer(jokes, many=True, context=request)
        return JsonResponse(serializer.data, safe=False)
    
    def post(self, request):
        serializer = JokeSerializer(data=request.data, context=request)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class JokeDetail(APIView):

    def get_joke(self, id):
        try:
            return Joke.objects.get(pk=id)
        except Joke.DoesNotExist:
            raise HttpResponse(status=status.HTTP_404_NOT_FOUND)
    
    def get(self, request, id):
        joke = self.get_joke(id)
        serializer = JokeSerializer(joke, context=request)
        return JsonResponse(serializer.data)
    
    def put(self, request, id):
        joke = self.get_joke(id)
        data = JSONParser().parse(request)
        serializer = JokeSerializer(joke, data=data, context=request)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        joke = self.get_joke(id)
        joke.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)