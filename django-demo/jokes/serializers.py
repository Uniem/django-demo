import uuid
from rest_framework import serializers
from jokes.models import Joke


class JokeSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField()
    class Meta:
        model = Joke
        fields = ['id', 'joke']

    def __init__(self, *args, **kwargs):
        method = kwargs['context'].method
        if(method == 'POST'):
            kwargs['data']['id'] = uuid.uuid4()
        elif(method == 'PUT'):
            kwargs['data']['id'] = kwargs['context'].path.split('/')[2]

        super(JokeSerializer, self).__init__(*args, **kwargs)