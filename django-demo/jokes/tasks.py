from gettext import find
import uuid

from .models import Joke

from .integration.jokes import getJoke
from main.celery import app as celery_app

@celery_app.task
def save_random_joke(arg):
    apiResponse = getJoke()
    id = apiResponse['id']
    try:
        joke = Joke.objects.get(external_id=id)
        joke.joke = apiResponse['joke']
    except Joke.DoesNotExist:
        joke = Joke(joke=apiResponse['joke'], id=uuid.uuid4(), external_id=apiResponse['id'])
    joke.save()