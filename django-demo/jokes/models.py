from django.db import models

class Joke(models.Model):
    id = models.UUIDField(primary_key=True)
    joke = models.TextField()
    external_id = models.CharField(max_length=15, blank=True, default='')

    class Meta:
        ordering = ['id']
