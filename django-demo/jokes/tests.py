import uuid
from django.test import TestCase

from .models import Joke

id = uuid.uuid4()

class JokeViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Joke.objects.create(
                id=id,
                joke='funny joke'
            )
            

    def test_view_get(self):
        response = self.client.get('/joke/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]['joke'], 'funny joke')

    def test_view_get_specific(self):
        getResponse = self.client.get(f'/joke/{id}/')
        self.assertEqual(getResponse.status_code, 200)
        self.assertEqual(getResponse.json()['joke'], 'funny joke')

    def test_view_put(self):
        response = self.client.put(f'/joke/{id}/', data={"joke": "new joke"}, content_type="application/json")
        self.assertEqual(response.status_code, 200)

        getResponse = self.client.get(f'/joke/{id}/')
        self.assertEqual(getResponse.status_code, 200)
        self.assertEqual(getResponse.json()['joke'], 'new joke')

    def test_view_post(self):
        response = self.client.post('/joke/', data={"joke": "another joke"}, content_type="application/json")
        self.assertEqual(response.status_code, 201)
        id = response.json()['id']
        getResponse = self.client.get(f'/joke/{id}/')
        self.assertEqual(getResponse.status_code, 200)
        self.assertEqual(getResponse.json()['joke'], 'another joke')

    def test_view_delete(self):
        response = self.client.delete(f'/joke/{id}/')
        self.assertEqual(response.status_code, 204)

        getResponse = self.client.get('/joke/')
        self.assertEqual(getResponse.status_code, 200)
        self.assertEqual(len(getResponse.json()), 0)

    

