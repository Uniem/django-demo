from django.urls import path
from jokes import views

urlpatterns = [
    path('joke/', views.JokeList.as_view()),
    path('joke/<uuid:id>/', views.JokeDetail.as_view()),
]