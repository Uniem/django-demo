## What does this program do

It continously pulls a random joke from https://icanhazdadjoke.com/ every 10 seconds and persists it to the database.
There is also support for CRUD for your own jokes (or deleting/modifying pulled jokes).

## How to run it

    docker-compose up

## How to run tests
    cd tests
    docker-compose run tests

## Example requests

Get jokes
    
    curl --silent 127.0.0.1:8000/joke/ | jq

Get specific joke
    
    curl --silent 127.0.0.1:8000/joke/4b50baec-e0f1-438a-86e7-5cfd7ca349a7/ | jq

Update joke
    
    curl --silent -X PUT \
    -H "Content-Type: application/json" \
    -d "{\"joke\": \"new joke\"}" \
    127.0.0.1:8000/joke/4b50baec-e0f1-438a-86e7-5cfd7ca349a7/ | jq

Create joke

    curl --silent -X POST \
    -H "Content-Type: application/json" \
    -d "{\"joke\": \"A funnier joke\"}" 127.0.0.1:8000/joke/ | jq

Delete joke

    curl --silent -X DELETE 127.0.0.1:8000/joke/4b50baec-e0f1-438a-86e7-5cfd7ca349a7/ | jq

